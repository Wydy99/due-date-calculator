<?php

/**
 * DueDateCalculator
 */
class DueDateCalculator
{
    /**
     * The start of work (e.g., 09:00).
     *
     * @var string
     */
    private $startOfWork = "09:00";

    /**
     * The end of work (e.g., 17:00).
     *
     * @var string
     */
    private $endOfWork = "17:00";

    /**
     * Constant array of days with parameters.
     *
     * @var array
     */
    private const DAYS = array(
        "Monday" => array('workday' => true),
        "Tuesday" => array('workday' => true),
        "Wednesday" => array('workday' => true),
        "Thursday" => array('workday' => true),
        "Friday" => array('workday' => true),
        "Saturday" => array('workday' => false),
        "Sunday" => array('workday' => false),
    );

    /**
     * The constructor of this class.
     *
     * @return void
     */
    public function __construct()
    {
        // Set private variables after function call with given parameters.
        $this->_startOfWorkInMinutes = $this->convertTimeToMinutes($this->startOfWork);
        $this->_endOfWorkInMinutes = $this->convertTimeToMinutes($this->endOfWork);
    }

    /**
     * Convert time to minutes.
     * (e.g., 09:00 ==> 540)
     *
     * @param  string $time
     * @return int 
     */
    private function convertTimeToMinutes(string $time): int
    {
        $minutes = 0;

        // Separate hours and minutes along the separator.
        $time = explode(':', $time);

        // Calculate minutes from hours and minutes.
        $minutes += $time[0] * 60 ?? 0;
        $minutes += $time[1] ?? 0;

        return $minutes;
    }

    /**
     * Get readable format of time from minutes.
     * (e.g., 540 ==> 09:00)
     *
     * @param  int $minutes
     * @return string 
     */
    private function getReadableTimeFromMinutes(int $minutes): string
    {
        // The whole and fractional part of division. 
        $hours = intdiv($minutes, 60);
        $minutes = fmod($minutes, 60);

        // Make a complete form with leading zeros.
        $hours = $hours < 10 ? "0{$hours}" : $hours;
        $minutes = $minutes < 10 ? "0{$minutes}" : $minutes;

        // Add a separator character.
        $readableTime = "{$hours}:{$minutes}";

        return $readableTime;
    }

    /**
     * Get workdays in array.
     *
     * @return array
     */
    private function getWorkdays(): array
    {
        // Filter days array by workday property.
        $workdays = array_filter(
            self::DAYS,
            function ($day) {
                return ($day['workday'] == true);
            }
        );
        $workdays = array_keys($workdays);

        return $workdays;
    }

    /**
     * Get next day.
     *
     * @param  string $day
     * @return string
     */
    private function getNextDay(string $day): string
    {
        // Get a key of this day from array.
        $numberOfDay = array_search($day, array_keys(self::DAYS));
        $numberOfDay = $numberOfDay == count(self::DAYS) - 1 ? 0 : $numberOfDay + 1;

        // Get a next day from array.
        $day = array_key_first(array_slice(self::DAYS, $numberOfDay, 1, true));

        return $day;
    }

    /**
     * Calculate due date according to the task description.   
     *
     * @param  string $day (e.g., Monday, Friday)
     * @param  string $time (e.g., 14:12)
     * @param  int $turnaroundTime (e.g., 16)
     * @return string
     */
    public function calculateDueDate(string $day, string $time, int $turnaroundTime): string
    {
        // Call a private member function to set these variables.
        $givenTimeInMinutes = $this->convertTimeToMinutes($time);
        $turnaroundTimeInMinutes = $this->convertTimeToMinutes($turnaroundTime);;

        // Set due day variable and minutes counter.
        $dueDay = $day;
        $dueTimeInMinutes = $givenTimeInMinutes;

        // Go until the last minute of turnaround time by minutes.
        for ($minute = 1; $minute <= $turnaroundTimeInMinutes; $minute++) {
            // Increase due time minutes counter.
            $dueTimeInMinutes++;
            // If due time minutes greater than or equal to end of work in minutes
            if ($dueTimeInMinutes >= $this->_endOfWorkInMinutes) {
                // Than change due day until it is not a workday.
                do {
                    // Get the next day.
                    $dueDay = $this->getNextDay($dueDay);
                } while (!in_array($dueDay, $this->getWorkdays()));

                // Set a start time of a new day.
                $dueTimeInMinutes = $this->_startOfWorkInMinutes;
            }
        }

        // Make readable format of result.
        $dueDate = $dueDay . ', ' . $this->getReadableTimeFromMinutes($dueTimeInMinutes);

        return $dueDate;
    }
}

// Make a new object.
$dueDateCalculator = new DueDateCalculator();

// Call a member function.
echo $dueDateCalculator->calculateDueDate("Tuesday", "14:12", 16);
